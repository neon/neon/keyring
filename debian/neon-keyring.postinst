#!/bin/sh

set -ex

ARCH=$(dpkg --print-architecture)

SUITE=$(grep DISTRIB_CODENAME= /etc/lsb-release | grep -oP '(?<=DISTRIB_CODENAME=).*')
# populate NEONARCHIVE var
# user edition with guards for release edition testing iso's
NEONARCHIVE_USER=$(grep -o User /etc/lsb-release | tr '[:upper:]' '[:lower:]' )
if [ "$NEONARCHIVE_USER" = "user" ];
    then NEONARCHIVE=$NEONARCHIVE_USER;
fi
# testing edition (in the future will need guards for stable edition testing iso's)
NEONARCHIVE_TESTING=$(grep -o Testing /etc/lsb-release | tr '[:upper:]' '[:lower:]' )
if [ "$NEONARCHIVE_TESTING" = "testing" ];
	then NEONARCHIVE=$NEONARCHIVE_TESTING;
fi
# unstable edition
NEONARCHIVE_UNSTABLE=$(grep -o Unstable /etc/lsb-release | tr '[:upper:]' '[:lower:]' )
if [ "$NEONARCHIVE_UNSTABLE" = "unstable" ];
	then NEONARCHIVE=$NEONARCHIVE_UNSTABLE;
fi

# move apt-key master key back to where it belongs if it was previously moved in unstable
if [ ! -e /etc/apt/trusted.gpg ] && [ -e /etc/apt/trusted.gpg.d/apt-trusted.gpg ]; then
	mv /etc/apt/trusted.gpg.d/apt-trusted.gpg /etc/apt/trusted.gpg
fi

# rm any neon keys from the apt-key keyring and update *.list
if [ "$1" = 'configure' -a -n "$2" ]; then
	# remove keys from the trusted.gpg file as they are now shipped in trusted.gpg.d
	if dpkg --compare-versions "$2" 'lt' "2024.08.09" && which gpg > /dev/null && which apt-key > /dev/null; then
		TRUSTEDFILE='/etc/apt/trusted.gpg'
		eval $(apt-config shell TRUSTEDFILE Apt::GPGV::TrustedKeyring)
		eval $(apt-config shell TRUSTEDFILE Dir::Etc::Trusted/f)
		if [ "$TRUSTEDFILE" ]; then
            # rm neon archive-signing-key
			for KEY in '444D ABCF 3667 D028 3F89  4EDD E6D4 7362 5575 1E5D'; do
				# rm the historic post-install copy to stop apt-key re-entering that keyring
				if [ -e /etc/apt/trusted.gpg.d/neon-archive-keyring ]; then
					rm /etc/apt/trusted.gpg.d/neon-archive-keyring.gpg
				fi
				# rm the the key from the apt-key trusted.gpg keyring
				apt-key --keyring "$TRUSTEDFILE" del $KEY > /dev/null 2>&1 || :
			done

			# rm ancient Ubuntu Local Archive One-Time Signing Key <cdimage@ubuntu.com>
			for KEY in '7B92 9DC5 3D6D 77FD 6427  45ED 1EC9 3359 A395 228C'; do
				# rm the historic post-install copy to stop apt-key re-entering that keyring
				if [ -e /etc/apt/trusted.gpg.d/ubuntu-local-archive-one-time-signing-key.gpg ]; then
					rm /etc/apt/trusted.gpg.d/ubuntu-local-archive-one-time-signing-key.gpg
				fi
				# rm the the key from the apt-key trusted.gpg keyring
				apt-key --keyring "$TRUSTEDFILE" del $KEY > /dev/null 2>&1 || :
			done
		fi
	fi

	# ensure all our *.lists are transitioned to *.sources
	# rm the old neon.list if it's still there
	if [ -e /etc/apt/sources.list.d/neon.list ]; then
		rm /etc/apt/sources.list.d/neon.list
	fi
	# add a nice depreciation message about the move to neon.sources
	echo "# KDE neon sources have moved to /etc/apt/sources.list.d/neon.sources" >> /etc/apt/sources.list.d/neon.list

	# rm the the neon.sources if it already exists
	if [ -e /etc/apt/sources.list.d/neon.sources ]; then
		rm /etc/apt/sources.list.d/neon.sources
	fi
	# add our spiffy new signed deb822 neon.sources that doesn't rely on apt-key
	cat > /etc/apt/sources.list.d/neon.sources << EOF
X-Repolib-Name: KDE neon $SUITE $NEONARCHIVE
Types: deb deb-src
URIs: http://archive.neon.kde.org/$NEONARCHIVE
Suites: $SUITE
Components: main
Signed-By: /etc/apt/keyrings/neon-archive-keyring.asc
EOF
	# Remove Architectures so it defaults to the dpkg default list
	sed -i '/Architectures/d' /etc/apt/sources.list.d/neon.sources

	# remove the old preinstalled-pool.list is it's still there
	if [ -e /etc/apt/sources.list.d/preinstalled-pool.list ]; then
		rm /etc/apt/sources.list.d/preinstalled-pool.list
	fi
	# install our new disabled deb822 preinstalled-pool.sources
	cat > /etc/apt/sources.list.d/preinstalled-pool.sources << EOF
Enabled: no
Types: deb
URIs: file:/var/lib/preinstalled-pool/
Suites: $SUITE
Components: main restricted universe multiverse
EOF

fi

#DEBHELPER#
